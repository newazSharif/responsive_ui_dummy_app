import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'content.dart';
import 'menu.dart';

//void main() => runApp(MyApp());
void main() => runApp(
  DevicePreview(

    builder: (context) => MyApp(),
  ),
);

class MyApp extends StatelessWidget {
  @override
  Widget build(context) =>
      MaterialApp(
//          locale: DevicePreview.of(context).locale, // <--- Add the locale
          builder: DevicePreview.appBuilder,
          home: MyHomePage()
      );
}

class MyHomePage extends StatelessWidget {

  @override
  Widget build(context) =>
      Scaffold(
          appBar: AppBar(title:Text("Responsive ui")),
          drawer: MediaQuery.of(context).size.width < 500 ?
          Drawer(
            child: Menu(),
          ) :
          null,
          body: SafeArea(
              child:Center(
                  child: MediaQuery.of(context).size.width < 500 ? Content() :
                  Row(
                      children: [
                        Container(
                            width: 200.0,
                            child: Menu()
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width-200.0,
                            child: Content()
                        )
                      ]
                  )
              )
          )
      );
}