import 'package:flutter/material.dart';

import 'LogInPage.dart';
class Menu extends StatelessWidget {
  @override
  Widget build(context) => ListView(
      children: [
        FlatButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginPage()));
            },
            child: ListTile(
              leading: Icon(Icons.star),
              title: Text("First item"),
            )
        ),
        FlatButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginPage()));
            },
            child: ListTile(
              leading: Icon(Icons.star),
              title: Text("Second item"),
            )
        )
      ]
  );
}